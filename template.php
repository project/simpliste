<?php

// Auto-rebuild the theme registry during theme development.
if (theme_get_setting('rebuild_registry')) {
  drupal_theme_rebuild();
}

/**
 * Implements hook_theme().
 */
function simpliste_theme() {
  return array(
    'search_element' => array('render element' => 'element'),
  );
}

/**
 * Implements hook_preprocess_html().
 */
function simpliste_preprocess_html(&$vars) {
  if ($head_code = theme_get_setting('head_code')) {
    drupal_add_html_head(array('#type' => 'markup', '#markup' => $head_code), 'simpliste_head_code');
  }
  if (theme_get_setting('wireframes')) {
    $vars['classes_array'][] = 'wireframes';
  }
  if (theme_get_setting('timer')) {
    drupal_register_shutdown_function('simpliste_shutdown');
  }
}


/**
 * Implements hook_preprocess_page().
 */
function simpliste_preprocess_page(&$vars) {

  // Add default skin.
  $skin = theme_get_setting('active_skin');
  drupal_add_css(
    $vars['directory'] . "/skins/$skin/style.css",
    array('weight' => 999, 'group' => CSS_THEME)
  );

  // Add PIE.
  $pie_path = $vars['base_path'] . $vars['directory'] . '/js/PIE.htc';
  $pie_selectors = '#main-menu li a, a.button, .form-submit, .tabs ul.primary li a';
  drupal_add_css($pie_selectors . '{position: relative; behavior: url(' . $pie_path . ');}',
    array(
      'type' => 'inline',
      'browsers' => array('IE' => 'lte IE 9', '!IE' => FALSE),
      'preprocess' => FALSE,
      'weight' => 1000,
      'group' => CSS_THEME,
    )
  );

  //-- Main menu.
  if (!empty($vars['main_menu'])) {
    $vars['main_menu'] = theme('links__system_main_menu', array(
      'links' => $vars['main_menu'],
      'attributes' => array(
        'class' => array('links', 'clearfix'),
      ),
      'heading' => array(
        'text' => t('Main menu'),
        'level' => 'h2',
        'class' => array('element-invisible'),
      )
    ));
  }
  else {
    $vars['main_menu'] = FALSE;
  }

  //-- Secondary menu.
  if (!empty($vars['secondary_menu'])) {
    $vars['secondary_menu'] = theme('links__system_secondary_menu', array(
      'links' => $vars['secondary_menu'],
      'attributes' => array(
        'class' => array('links', 'inline', 'clearfix'),
      ),
      'heading' => array(
        'text' => t('Secondary menu'),
        'level' => 'h2',
        'class' => array('element-invisible'),
      )
    ));
  }
  else {
    $vars['secondary_menu'] = FALSE;
  }

  //-- Sidebar position.
  $vars['sidebar_position'] = theme_get_setting('sidebar_position');
  
  //-- Sidebar width.
  $vars['content_classes'] = $vars['page']['sidebar'] ? 'col_66' : 'col_100';

  //-- Footer message.
  $vars['footer_message'] = theme_get_setting('footer_message');
  
  //-- Simpliste settings.
  if (theme_get_setting('settings_form') && $vars['page']['sidebar']) {
    $vars['page']['sidebar']['settings_form'] = drupal_get_form('simpliste_settings_form');
    $vars['page']['sidebar']['#sorted'] = FALSE;
  }
}

/**
 * Implements hook_preprocess_node().
 */
function simpliste_preprocess_node(&$vars) {

}

/**
 * Implements hook_preprocess_region().
 */
function simpliste_preprocess_region(&$vars) {

}

/**
 * Implements hook_preprocess_comment().
 */
function simpliste_preprocess_comment(&$vars) {
  $vars['id'] = l(
    '#' . $vars['id'],
    'node/' . $vars['node']->nid,
    array(
      'attributes' => array('class' => array('comment-id')),
      'fragment' => 'comment-' . $vars['comment']->cid,
    )
  );
}

/**
 * Implements hook_preprocess_block().
 */
function simpliste_preprocess_block(&$vars) {
  if ($vars['block']->region == 'featured') {
    $vars['classes_array'][] = 'col_33';
  }
}


/**
 * Implements theme_region().
 */
function simpliste_region($vars) {
  return $vars['content'];
}

/**
 * Implements theme_breadcrumb().
 */
function simpliste_breadcrumb($vars) {
  $breadcrumb_settings = theme_get_setting('breadcrumb');
  if ($breadcrumb_settings['display']) {
    // Optionally get rid of the homepage link.
    if (!$breadcrumb_settings['home']) {
      array_shift($vars['breadcrumb']);
    }
    // Append the content title to the end of the breadcrumb.
    if ($breadcrumb_settings['title']) {
      $vars['breadcrumb'][] = drupal_get_title();
    }
    // Return the breadcrumb with separators.
    if ($vars['breadcrumb']) {
      return implode($breadcrumb_settings['separator'], $vars['breadcrumb']);
    }
  }
  return FALSE;
}

/**
 * Implements theme_block__system__main().
 */
function simpliste_block__system__main($vars) {
  $output = '<div id="system-main" class="' . $vars['classes'] . '" ' . $vars['attributes'] . '>';
  $output .= render($title_prefix);
  if ($vars['block']->subject) {
    $output .= '<h2 ' . $vars['title_attributes'] . '>' . $vars['block']->subject . '</h2>';
  }
  $output .= render($vars['title_suffix']);
  $output .= $vars['content'];
  $output .= '</div>';
  return $output;
}


/**
 * Implements hook_form_BASE_FORM_ID_alter().
 */
function simpliste_form_node_form_alter(&$form, $form_state) {
  if (theme_get_setting('hide_format_node_form')) {
    $form['#after_build'][] = 'simpliste_node_hide_format_selection';
  }
}

/**
 * Node form after_build callback.
 */
function simpliste_node_hide_format_selection($form) {
  $form['body'][LANGUAGE_NONE]['0']['format']['#access'] = FALSE;
  return $form;
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function simpliste_form_comment_form_alter(&$form, $form_state) {
  if (theme_get_setting('hide_format_comment_form')) {
    $form['#after_build'][] = 'simpliste_comment_hide_format_selection';
  }
}

/**
 * Comment form after_build callback.
 */
function simpliste_comment_hide_format_selection($form) {
  $form['comment_body'][LANGUAGE_NONE]['0']['format']['#access'] = FALSE;
  return $form;
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function simpliste_form_search_block_form_alter(&$form, $form_state) {
  $form['search_block_form']['#theme'] = 'search_element';
  $form['search_block_form']['#attributes']['type'] = 'search';
  $form['search_block_form']['#attributes']['placeholder'] = t('Search this site');
  $form['search_block_form']['#size'] = 25;
}

/**
 * Implements theme_search_element().
 */
function simpliste_search_element($vars) {
  $element = $vars['element'];
  element_set_attributes($element, array('id', 'name', 'value', 'size', 'maxlength'));
  $output = '<input' . drupal_attributes($element['#attributes']) . ' />';
  return $output;
}


/**
 * Implements theme_field__field_type().
 */
function simpliste_field__text_with_summary($vars) {

  $output = '';

  // Render the label, if it's not hidden.
  if (!$vars['label_hidden']) {
    $output .= '<div class="field-label"' . $vars['title_attributes'] . '>' . $vars['label'] . ':&nbsp;</div>';
  }

  // Render the items.
  foreach ($vars['items'] as $delta => $item) {
    $classes = 'field-item ' . ($delta % 2 ? 'odd' : 'even');
    $output .= '<div class="body ' . $classes . '"' . $vars['item_attributes'][$delta] . '>' . drupal_render($item) . '</div>';
  }
  return $output;
}


/**
 * Simpliste settings form.
 */
function simpliste_settings_form($form, &$form_state) {
  $form = array(
    '#prefix' => '<div class="block"><h2>' . t('Simpliste settings') . '</h2>',
    '#suffix' => '</div>',
    'active_skin' => array(
      '#type' => 'select',
      '#title' => t('Active skin:'),
      '#options' => drupal_map_assoc(simpliste_get_skins()),
      '#default_value' => theme_get_setting('active_skin'),
    ),
    'sidebar_position' => array(
      '#type' => 'select',
      '#title' => t('Sidebar position:'),
      '#options' => drupal_map_assoc(array('left', 'right')),
      '#default_value' => theme_get_setting('sidebar_position'),
    ),
    'wireframes' => array(
      '#type' => 'checkbox',
      '#title' => t('Wireframe mode'),
      '#default_value' => theme_get_setting('wireframes'),
    ),
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Send'),
    ),
  );
  return $form;
}

/**
 * Simpliste settings form submit.
 */
function simpliste_settings_form_submit($form, &$form_state) {
  $settings = variable_get('theme_simpliste_settings', array());
  $settings['active_skin'] = $form_state['values']['active_skin'];
  $settings['sidebar_position'] = $form_state['values']['sidebar_position'];
  $settings['wireframes'] = $form_state['values']['wireframes'];
  variable_set('theme_simpliste_settings', $settings);
}

/**
 * Get all skins.
 *
 *  @TODO Make it cacheable.
 */
function simpliste_get_skins() {
  foreach (scandir(drupal_get_path('theme', 'simpliste') . '/skins') as $directory) {
    if ($directory != '.' && $directory != '..') {
      $skins[] = $directory;
    }
  }
  return $skins;
}

/**
 * Shutdown callback.
 */
function simpliste_shutdown() {
  echo '<div id="timer">' . t('Page execution time was <b>!time</b> ms' , array('!time' => round(timer_read('page')))) . '</div>';
}